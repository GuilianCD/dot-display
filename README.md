
# Dot Display

Showcase your dots simply by using an orphaned git branch of your config's git !

## How it works

1. Create an orphaned branch named `dot-display`
2. Put the necessary files in it :
    - Add a `README.md` to talk about whatever you want
    - Add a `config.json` to add metadata (the theme, the DE/WM, etc.)
    - Add a `media/` folder for all your pics and videos and what not
3. Go to `DOT-DISPLAY-URL/dots/<your name>/<your dots repo>` and see it live !

By using another branch (that you will rarely edit anyway), your config and it's 
nifty presentation you made live together, but do not make trouble for each other !


Access the [test repository](https://gitlab.com/GuilianCD/fake-dotfiles)
