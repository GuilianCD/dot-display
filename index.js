import { Database } from "bun:sqlite";
import express from "express";



const db = new Database("mydb.sqlite", { create: true });

process.on('exit', () => {
	db.close();
})



const app = express();

app.use(express.json());
app.use('/static', express.static('public'))
app.set('view engine', 'ejs');



async function get_gitlab_info(repo_slug) {
	const [readmeRes, configRes] = await Promise.all([
		fetch(`https://gitlab.com/${repo_slug}/-/raw/dot-display/README.md`), 
		fetch(`https://gitlab.com/${repo_slug}/-/raw/dot-display/config.json`)
	]);
	
	const [readme, config] = await Promise.all([ readmeRes.text(), configRes.json() ])
	
	return {
		readme,
		config
	}
}


app.get('/dots/:user/:repo', async (req, res) => {
	const user = req.params.user;
	const repo = req.params.repo;

	const repo_slug = `${user}/${repo}`;
	const repo_url  = `https://gitlab.com/${repo_slug}`


	const {readme, config} = await get_gitlab_info(repo_slug)

	const operating_system = config.OS ?? 'OS_Unknown';
	const environment = config.environment ?? 'ENV_Unknown';
	const font = config.font ?? 'FONT_Unknown';
	const media = (config.media ?? []).map( media => `https://gitlab.com/${repo_slug}/-/raw/dot-display/media/${media}` ).at(0);

  res.render('index', {
		data: {
			repo_url,
			repo_slug,
			author: user,

			operating_system,
			environment,
			font
		},

		body: readme,
		media
	} );
});


app.get('/register', (req, res) => {
	res.render('register');
});

app.post('/register/submit', (req, res) => {
	const args = req.body;

	if (args.username === undefined || args.password === undefined) {
		res.send(`
			<h2 class="text-red-600">Error !</h2>
			<p>You need to provide both a password and a username to register.</p>
			<a href="/register" class="w-fit bg-red-800 hover:bg-red-900 px-4 py-2 rounded-md block mx-auto mt-8 text-lg text-white">Try again ?</a>
		`);
	}
});



app.listen(8080);
console.log('Server is listening on http://127.0.0.1:8080');

